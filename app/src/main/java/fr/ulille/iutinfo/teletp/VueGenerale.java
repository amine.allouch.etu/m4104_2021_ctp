package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String DISTANCIEL;
    private String salle ;
    private String poste;
    // TODO Q2.c
    public SuiviViewModel suiviViewModel;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        poste = "";
        DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        salle = DISTANCIEL;
        // TODO Q2.c
        suiviViewModel = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = getActivity().findViewById(R.id.spSalle);
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);

        ArrayAdapter<CharSequence> arrayAdapterSalle=  ArrayAdapter.createFromResource(getContext(), R.array.list_salles, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> arrayAdapterPoste=  ArrayAdapter.createFromResource(getContext(), R.array.list_postes, android.R.layout.simple_spinner_item);

        spSalle.setAdapter(arrayAdapterSalle);
        spPoste.setAdapter(arrayAdapterPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
            TextView tvLogin = getActivity().findViewById(R.id.tvLogin);
            suiviViewModel.setUsername(tvLogin.getText().toString());
        });

        // TODO Q5.b
        //spSalle.setOnItemClickListener()

        update();
        // TODO Q9
    }

    // TODO Q5.a
    public void update(){
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);

        if(salle.equals("Distanciel")){
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(false);
            suiviViewModel.setLocalisation("Distanciel");
        }
        else {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(true);
            suiviViewModel.setLocalisation(salle + " : " + poste);
        }

    }

    // TODO Q9
}